<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Search Artist By Country</title>
<script type="text/javascript" src="js/angular.min.js"></script>

<script>
var app = angular.module('myApp', [])
			.controller('MyController', MyController);
function MyController($scope, $http) {

        $scope.init = function() {
        		console.info("inside ajax call");
                $http({
                        method : 'GET',
                        url : 'MusicServlet',
                        params: {country: $scope.country, page:$scope.page}
                }).then(function (response, status) {
                	$scope.allData = response.data;
                	if($scope.allData.topartists){
                		$scope.artists = $scope.allData.topartists.artist;
                		$scope.pageData = $scope.allData.topartists["@attr"];
                		$scope.tp = [];
//                 		 for(var i=1;i<$scope.pageData.totalPages;i++){
//                 		        $scope.tp.push(i);
                			 
//                 		 }
                		
                	}
				}, function errorCallback(response) {
					
				});
				
               
				
        };
        
        
        
};
</script>
</head>
<body>
	<div ng-app="myApp">
		<ul ng-controller="MyController">
			<label>Enter Country: <input type="text" ng-model="country"
				value="india" required />
			</label>
			<button ng-click="init()">Search</button>

			<li ng-repeat="artist in artists"><input type="hidden"
				ng-model="mbid" value={{artist.mbid}} />
				<p>
					{{artist.name}} <a href='ArtistServlet?mbid={{artist.mbid}}'
						target="_blank"><img ng-src='{{artist.image[0]["#text"]}}'></img></a>
				</p></li>
			<p ng-repeat='item in tp'>
				<input type="hidden"
				ng-model="page" value={{item}} />
				<a href="#" ng-click="init()">{{item}}</a>
			</p>

		</ul>

	</div>
</body>
</html>