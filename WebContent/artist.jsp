<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Artist Top 5 Tracks</title>
<script type="text/javascript" src="js/angular.min.js"></script>
<script>

</script>
</head>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<body>
	<div >
		<h1>Top 5 Tracks</h1>
		<ul>
				<c:forEach items="${artistTracks.toptracks.track}" var="item">
					<li >
				    	<c:out value="${item.name}" />
					</li>
				</c:forEach>
			
			
		</ul>
		
	</div>
</body>
</html>