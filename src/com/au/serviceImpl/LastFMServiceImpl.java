package com.au.serviceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.au.service.LastFMService;

public class LastFMServiceImpl implements LastFMService{

	@Override
	public String search(String keyword,String limit,String page) {
		StringBuffer sb = new StringBuffer();
		  try {

			  	String host = "http://ws.audioscrobbler.com/2.0/?";
			  	String method = "method=geo.gettopartists";
			  	String urlParams = "&country="+keyword+"&api_key=e7f9b2c674c9c4981f5554c51f1f7948&format=json&limit=5&page="+page;
				//URL url = new URL("http://ws.audioscrobbler.com/2.0/?method=geo.gettopartists&country=spain&api_key=e7f9b2c674c9c4981f5554c51f1f7948&format=json&limit=5");
			  	URL url = new URL(host+method+urlParams);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/json");

				if (conn.getResponseCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
							+ conn.getResponseCode());
				}

				BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

				String output;
				
				while ((output = br.readLine()) != null) {
					sb.append(output);
					System.out.println(output);
				}

				conn.disconnect();

			  } catch (MalformedURLException e) {

				e.printStackTrace();

			  } catch (IOException e) {

				e.printStackTrace();

			  }
		  
		  return sb.toString();
		
	}

	@Override
	public String getArtistTopTracks(String mbid, String limit) {

		StringBuffer sb = new StringBuffer();
		  try {

			  	String host = "http://ws.audioscrobbler.com/2.0/?";
			  	String method = "method=artist.gettoptracks";
			  	String urlParams = "&mbid="+mbid+"&api_key=e7f9b2c674c9c4981f5554c51f1f7948&format=json&limit=5";
				//URL url = new URL("http://ws.audioscrobbler.com/2.0/?method=geo.gettopartists&country=spain&api_key=e7f9b2c674c9c4981f5554c51f1f7948&format=json&limit=5");
			  	URL url = new URL(host+method+urlParams);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/json");

				if (conn.getResponseCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
							+ conn.getResponseCode());
				}

				BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

				String output;
				while ((output = br.readLine()) != null) {
					sb.append(output);
					System.out.println(output);
				}

				conn.disconnect();

			  } catch (MalformedURLException e) {

				e.printStackTrace();

			  } catch (IOException e) {

				e.printStackTrace();

			  }
		  
		  return sb.toString();
		
	
	}

}
