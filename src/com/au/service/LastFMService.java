package com.au.service;

public interface LastFMService {
	
	public String search(String keyword,String limit,String page);
	
	public String getArtistTopTracks(String mbid,String limit);

}
